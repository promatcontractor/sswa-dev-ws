USE [master]
GO



/****** Object:  Database [SSWA_ShiftLog]    Script Date: 25/08/2018 2:06:16 PM *********
*******  NOTE: Change directory to suit                                            ******/
CREATE DATABASE [SSWA_ShiftLog] ON  PRIMARY 
( NAME = N'SSWA_ShiftLog', FILENAME = N'E:\MSSQL\Data\SSWA_ShiftLog.mdf' , SIZE = 21504KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SSWA_ShiftLog_log', FILENAME = N'E:\MSSQL\Data\SSWA_ShiftLog_log.ldf' , SIZE = 24384KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO









USE [SSWA_ShiftLog]
GO
/****** Object:  Table [dbo].[Area]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Area](
	[AreaID] [varchar](20) NOT NULL,
	[Area] [varchar](100) NOT NULL,
	[Title] [varchar](255) NULL,
	[Comments] [varchar](255) NULL,
	[UseArea] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[AreaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Equipment]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Equipment](
	[TAG] [varchar](50) NOT NULL,
	[Area] [varchar](20) NULL,
	[Description] [varchar](255) NULL,
	[PID] [varchar](50) NULL,
	[PREFIX] [varchar](20) NULL,
	[INUM] [varchar](20) NULL,
	[Type] [varchar](20) NULL,
	[Notes] [varchar](255) NULL,
	[ProjectId] [int] NULL,
	[TAG_Original] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[TAG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InspectionItemOptions]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InspectionItemOptions](
	[InspectionItemId] [int] NOT NULL,
	[SelectionOption] [varchar](255) NOT NULL,
 CONSTRAINT [PK_InspectionItemOptions] PRIMARY KEY CLUSTERED 
(
	[InspectionItemId] ASC,
	[SelectionOption] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InspectionItems]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InspectionItems](
	[Id] [int] NOT NULL,
	[Area] [varchar](50) NOT NULL,
	[InspectionItemName] [varchar](255) NOT NULL,
	[DataType] [varchar](50) NOT NULL,
 CONSTRAINT [PK_InspectionItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InspectionLog]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InspectionLog](
	[ShiftId] [int] NOT NULL,
	[InspectionItemId] [int] NOT NULL,
	[Value] [varchar](max) NOT NULL,
 CONSTRAINT [PK_InspectionLog] PRIMARY KEY CLUSTERED 
(
	[ShiftId] ASC,
	[InspectionItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PIDMaster]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PIDMaster](
	[PID] [varchar](20) NOT NULL,
	[PID_Title] [varchar](255) NOT NULL,
	[Revision] [varchar](255) NULL,
	[Date_Modified] [datetime] NULL,
	[Notes] [varchar](255) NULL,
	[ProjectId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PlantSystem]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlantSystem](
	[Area] [varchar](20) NOT NULL,
	[SystemID] [varchar](20) NOT NULL,
	[SystemName] [varchar](255) NULL,
	[Notes] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Area] ASC,
	[SystemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Project]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Project](
	[Id] [uniqueidentifier] NOT NULL,
	[Reference] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Projects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ShiftLog_Detail]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ShiftLog_Detail](
	[LogID] [int] NOT NULL,
	[ShiftLogID] [int] NOT NULL,
	[Time] [time](7) NOT NULL,
	[Operator] [int] NOT NULL,
	[AreaID] [varchar](20) NULL,
	[Tag] [varchar](50) NULL,
	[PID] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
	[MaintenanceRequired] [bit] NULL,
	[MaintenanceDescription] [varchar](max) NULL,
	[MAXIMORefNo] [varchar](255) NULL,
	[Comments] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ShiftLog_Master]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ShiftLog_Master](
	[ShiftID] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[ShiftType] [varchar](10) NOT NULL,
	[ReviewedBy] [int] NULL,
	[Swell] [float] NULL,
	[MaxFCastWindSpeed] [float] NULL,
	[LowTideTime] [time](7) NULL,
	[Rain_Forecast] [float] NULL,
	[Comments] [varchar](max) NULL,
	[OHSE_Comments] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ShiftID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ShiftLog_Operator_Comments]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ShiftLog_Operator_Comments](
	[ShiftID] [int] NOT NULL,
	[Operator] [int] NOT NULL,
	[ReviewedBy] [int] NULL,
	[Comments] [varchar](max) NULL,
	[OHSE_Comments] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ShiftID] ASC,
	[Operator] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [nvarchar](64) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Reference] [int] NULL,
	[Operator] [bit] NOT NULL,
	[Admin] [bit] NOT NULL,
	[Approver] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[v_DailyLogDetails]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[v_DailyLogDetails]
As
/****************************************************************************
Select * From v_DailyLogDetails Order By Date, ShiftType,ShiftID,Time,LogID
*****************************************************************************/

Select m.ShiftID, m.Date, m.ShiftType,m.OperatorId As ShiftCreatedBy,m.Comments, d.Operator, d.LogID,d.Time, d.AreaID, a.Area, d.Tag,e.Description As Equipment, d.PID, p.PID_Title, d.Description, d.Comments As LineComments ,d.MaintenanceRequired, d.MaintenanceDescription, d.MAXIMORefNo
From ShiftLog_Master m
Left Join ShiftLog_Detail d On m.ShiftID = d.[ShiftLogID]
Left Join Area a On d.AreaID = a.AreaID
Left Join Equipment e On d.Tag = e.TAG
Left Join PIDMaster p On e.PID = p.PID



WHere m.ShiftID>='8000'
GO
/****** Object:  View [dbo].[v_Tags]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
Create view [dbo].[v_Tags]
As

SELECT Distinct [TAG]
      
      ,[Description]
      
  FROM [SSWA_ShiftLog].[dbo].[Equipment] where [Description]!=''
GO
ALTER TABLE [dbo].[InspectionItemOptions]  WITH CHECK ADD  CONSTRAINT [FK_InspectionItemOptions_InspectionItems] FOREIGN KEY([InspectionItemId])
REFERENCES [dbo].[InspectionItems] ([Id])
GO
ALTER TABLE [dbo].[InspectionItemOptions] CHECK CONSTRAINT [FK_InspectionItemOptions_InspectionItems]
GO
ALTER TABLE [dbo].[ShiftLog_Detail]  WITH CHECK ADD  CONSTRAINT [FK_ShiftLog_Detail_Master1] FOREIGN KEY([ShiftLogID])
REFERENCES [dbo].[ShiftLog_Master] ([ShiftID])
GO
ALTER TABLE [dbo].[ShiftLog_Detail] CHECK CONSTRAINT [FK_ShiftLog_Detail_Master1]
GO
ALTER TABLE [dbo].[ShiftLog_Operator_Comments]  WITH CHECK ADD  CONSTRAINT [FK_ShiftLog_Operator_Comments_ShiftLog_Master] FOREIGN KEY([ShiftID])
REFERENCES [dbo].[ShiftLog_Master] ([ShiftID])
GO
ALTER TABLE [dbo].[ShiftLog_Operator_Comments] CHECK CONSTRAINT [FK_ShiftLog_Operator_Comments_ShiftLog_Master]
GO
/****** Object:  StoredProcedure [dbo].[proc_InsertShift_Detail]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/


Create procedure [dbo].[proc_InsertShift_Detail]
  @ShiftLogID int,
  @AreaID varchar(25),
  @Tag varchar(25),
  @PID varchar(25),
  @Description varchar(255),
  @Comment varchar(255)

 
  As

  Declare 
  -- @ShiftLogID int,
  @NewLogID int,  
  @ShiftTime time(7),
  @Operator int

  --Set  @ShiftLogID=8167
  

  Set   @ShiftTime=cast(getdate() as time)  
  Set  @Operator= (Select [OperatorId] from [SSWA_ShiftLog].[dbo].[ShiftLog_Master] where [ShiftID]= @ShiftLogID)
  Set @NewLogID=(Select Max(LogID)+1 as Id  from  [SSWA_ShiftLog].[dbo].[ShiftLog_Detail])

  
  If(Select Count(*) from [SSWA_ShiftLog].[dbo].[ShiftLog_Detail] Where Operator=@Operator And time=@ShiftTime And ShiftLogID=@ShiftLogID)<=0
  Begin

  Insert Into [SSWA_ShiftLog].[dbo].[ShiftLog_Detail]([LogID],[ShiftLogID],[Time],[Operator],[AreaID],[Tag],[PID],[Description],[MaintenanceRequired],[MaintenanceDescription],[MAXIMORefNo],[Comments])
  Select   @NewLogID,@ShiftLogID,@ShiftTime,@Operator,@AreaID,@Tag,@PID,@Description,0,'','',@Comment

  Select @NewLogID

  End


  
GO
/****** Object:  StoredProcedure [dbo].[proc_InsertShift_Master]    Script Date: 25/08/2018 2:08:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

--Execute  [dbo].[proc_InsertShift_Master] 101,'Day'

  CREATE procedure [dbo].[proc_InsertShift_Master]
  @OperatorID int,
  @ShiftType varchar(25),
  @Swell Varchar(25),
  @MaxFCastWindSpeed Varchar(25),
  @LowTideTime Varchar(25),
  @Rain_Forecast  Varchar(5),
  @Comments varchar(100),
  @OHSE_Comments varchar(100),
  @LogDate varchar(50)

   
     
       
    
 
  As

  Declare @NewShiftID int,  @ShiftDate Date,@CheckRain_Forecast Int
  --,@ShiftType varchar(25), @OperatorID int
  Set   @ShiftDate=getdate()
  --Set @OperatorID=101
  --Set @ShiftType='Day'

  --If( @Rain_Forecast='Yes')
  --Set @CheckRain_Forecast=1
  --If( @Rain_Forecast='No')
  --  Set @CheckRain_Forecast=0

  Set @NewShiftID=(Select Max(ShiftID)+1 as Id  from  [SSWA_ShiftLog].[dbo].[ShiftLog_Master])

 

  If(Select Count(*) from [SSWA_ShiftLog].[dbo].[ShiftLog_Master] Where Date=@ShiftDate And ShiftType=@ShiftType And OperatorId=@OperatorID)<=0
  Begin

  Insert Into [SSWA_ShiftLog].[dbo].[ShiftLog_Master]([ShiftID],[Date],[OperatorId] ,[ShiftType],[ReviewedBy],[Swell],[MaxFCastWindSpeed],[LowTideTime],[Rain_Forecast],[Comments],[OHSE_Comments],[ProjectId])
  Select @NewShiftID,@LogDate,@OperatorID,@ShiftType,0,@Swell,@MaxFCastWindSpeed,null, @Rain_Forecast,@Comments,@OHSE_Comments,1

 Select @NewShiftID

  End


  
GO


