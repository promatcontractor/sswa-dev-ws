﻿using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application;
using SSWA.Local.Application.UseCases.UpdateUser;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.UpdateUser
{
    [Route("[controller]")]
    public class UsersController : Controller
    {
        private readonly IUpdateUserUseCase _updateUserUseCase;
        private readonly Presenter _presenter;

        public UsersController(
            IUpdateUserUseCase updateUserUseCase,
            Presenter presenter)
        {
            _updateUserUseCase = updateUserUseCase;
            _presenter = presenter;
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateUser(UserOutput user)
        {
            RolesCollection roles = new RolesCollection();
            foreach (var role in user.Roles.Where(x => x.Value))
            {
                roles.Add(role.Key.ToLowerInvariant());
            }
            User updateUser = Core.Users.User.Load(user.UserId, null, user.Name, roles);
            await _updateUserUseCase.Execute(updateUser);
            _presenter.Populate(updateUser, this);
            return _presenter.ViewModel;
        }
    }
}
