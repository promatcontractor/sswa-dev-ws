﻿using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.GetUsers;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.GetUsers
{
    [Route("[controller]")]
    public class UsersController : Controller
    {
        private readonly IGetUsersUseCase _getUsersUseCase;
        private readonly Presenter _presenter;

        public UsersController(
            IGetUsersUseCase getUserUseCase,
            Presenter presenter)
        {
            _getUsersUseCase = getUserUseCase;
            _presenter = presenter;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetUsers()
        {
            IReadOnlyCollection<User> users = await _getUsersUseCase.Execute();
            _presenter.Populate(users, this);
            return _presenter.ViewModel;
        }
    }
}
