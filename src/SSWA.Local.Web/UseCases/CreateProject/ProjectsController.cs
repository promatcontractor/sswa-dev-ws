﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.CreateProject;

namespace SSWA.Local.Web.UseCases.CreateProject
{
    [Authorize]
    [Route("[controller]")]
    public sealed class ProjectsController : Controller
    {
        private readonly ICreateProjectUseCase _createProjectUseCase;
        private readonly Presenter _presenter;

        public ProjectsController(
            ICreateProjectUseCase createProjectUseCase,
            Presenter presenter)
        {
            _createProjectUseCase = createProjectUseCase;
            _presenter = presenter;
        }

        [HttpGet]
        public IActionResult CreateProject()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateProject(CreateProjectRequest request)
        {
            if (!ModelState.IsValid)
                return View();

            CreateProjectOutput output = await _createProjectUseCase.Execute(request.Name);

            _presenter.Populate(output);

            return _presenter.ViewModel;
        }

    }
}