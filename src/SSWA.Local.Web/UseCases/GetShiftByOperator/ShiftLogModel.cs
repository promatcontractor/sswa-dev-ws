﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.GetShiftByOperator
{
    public sealed class ShiftLogModel
    {
        public Guid LogId { get; }

        public string OperatorName { get; }

        public TimeSpan Time { get; }

        public string AreaId { get; }

        public string Tag { get; }

        public string PID { get; }

        public string Description { get; }

        public bool? MaintenanceRequired { get; }

        public string MaintenanceDescription { get; }

        public string MaximoRefNo { get; }

        public string Comments { get; }

        public ShiftLogModel(
            Guid logId,
            string operatorName,
            TimeSpan time,
            string areaId,
            string tag,
            string pid,
            string decription,
            bool? maintenanceRequired,
            string maintenanceDescription,
            string maximoRefNo,
            string comments)
        {
            LogId = logId;
            OperatorName = operatorName;
            Time = time;
            AreaId = areaId;
            Tag = tag;
            PID = pid;
            Description = Description;
            MaintenanceRequired = maintenanceRequired;
            MaintenanceDescription = maintenanceDescription;
            MaximoRefNo = maximoRefNo;
            Comments = comments;
        }
    }
}
