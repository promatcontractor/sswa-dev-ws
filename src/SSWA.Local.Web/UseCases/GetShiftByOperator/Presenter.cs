﻿using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.GetShiftByOperator
{
    public sealed class Presenter
    {
        public IActionResult ViewModel { get; private set; }

        public void Populate(ShiftOutput output, Controller controller)
        {
            List<ShiftLogModel> logs = new List<ShiftLogModel>();

            foreach (var item in output.Logs)
            {
                var log = new ShiftLogModel(
                    item.LogId,
                    item.Operator.Name,
                    item.Time,
                    item.AreaId,
                    item.Tag,
                    item.PID,
                    item.Description,
                    item.MaintenanceRequired,
                    item.MaintenanceDescription,
                    item.MaximoRefNo,
                    item.Comments
                    );

                logs.Add(log);
            }

            ShiftModel shift = new ShiftModel(
                output.ShiftId,
                output.Date,
                output.ShiftType,
                output.Swell,
                output.MaxForecaseWindSpeed,
                output.LowTideTime,
                output.RainForecast,
                output.Comments,
                output.OHSEComments,
                output.Operator.Name,
                logs
                );

            ViewModel = controller.View(shift);
        }
    }
}
