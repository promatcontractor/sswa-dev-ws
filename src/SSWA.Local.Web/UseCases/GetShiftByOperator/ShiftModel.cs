﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.GetShiftByOperator
{
    public sealed class ShiftModel
    {
        public Guid ShiftId { get; }

        public DateTime Date { get; }

        public string ShiftType { get; }

        public string OperatorName { get; }

        public decimal? Swell { get; }

        public decimal? MaxForecaseWindSpeed { get; }

        public TimeSpan? LowTideTime { get; }

        public decimal? RainForecast { get; }
        
        public string Comments { get; }

        public string OHSEComments { get; }

        public List<ShiftLogModel> Logs { get; }

        public ShiftModel(
            Guid shiftId,
            DateTime date,
            string shiftType,
            decimal? swell,
            decimal? maxForecastWindSpeed,
            TimeSpan? lowTideTime,
            decimal? rainForecast,
            string comments,
            string ohseComments,
            string @operatorName,
            List<ShiftLogModel> logs)
        {
            ShiftId = shiftId;
            Date = date;
            ShiftType = shiftType;
            Swell = swell;
            MaxForecaseWindSpeed = maxForecastWindSpeed;
            LowTideTime = lowTideTime;
            RainForecast = rainForecast;
            Comments = comments;
            OHSEComments = ohseComments;
            OperatorName = operatorName;
            Logs = logs;
        }

    }
}
