﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application;
using SSWA.Local.Application.UseCases.GetShiftByOperator;

namespace SSWA.Local.Web.UseCases.GetShiftByOperator
{
    [Route("[controller]")]
    public class ShiftsController : Controller
    {
        private readonly IGetShiftByOperatorUseCase _useCase;
        private readonly Presenter _presenter;

        public ShiftsController(
            IGetShiftByOperatorUseCase useCase,
            Presenter presenter)
        {
            this._useCase = useCase;
            this._presenter = presenter;
        }

        [HttpGet(""), Authorize]
        public async Task<IActionResult> GetShiftByOperator()
        {
            ShiftOutput output = await _useCase.Execute(null, null, null);
            _presenter.Populate(output, this);
            return _presenter.ViewModel;
        }

        [HttpGet("{date}")]
        public async Task<IActionResult> GetShiftByOperator(DateTime? date)
        {
            ShiftOutput output = await _useCase.Execute(date, null, null);
            _presenter.Populate(output, this);
            return _presenter.ViewModel;
        }

        [HttpGet("{date}/{shiftType}")]
        public async Task<IActionResult> GetShiftByOperator(DateTime? date, string shiftType)
        {
            ShiftOutput output = await _useCase.Execute(date, shiftType, null);
            _presenter.Populate(output, this);
            return _presenter.ViewModel;
        }

        [HttpGet("{date}/{shiftType}/{operatorId}")]
        public async Task<IActionResult> GetShiftByOperator(DateTime? date, string shiftType, Guid? operatorId)
        {
            ShiftOutput output = await _useCase.Execute(date, shiftType, operatorId);
            _presenter.Populate(output, this);
            return _presenter.ViewModel;
        }
    }
}