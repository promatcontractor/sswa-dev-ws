﻿using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.GetUser;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.GetUser
{
    [Route("[controller]")]
    public class UsersController : Controller
    {
        private readonly IGetUserUseCase _getUserUseCase;
        private readonly Presenter _presenter;

        public UsersController(
            IGetUserUseCase getUserUseCase,
            Presenter presenter)
        {
            _getUserUseCase = getUserUseCase;
            _presenter = presenter;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(string id)
        {
            User user = await _getUserUseCase.Execute(id);
            _presenter.Populate(user, this);
            return _presenter.ViewModel;
        }
    }
}
