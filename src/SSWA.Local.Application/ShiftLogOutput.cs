﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Application
{
    public sealed class ShiftLogOutput
    {
        public Guid LogId { get;  }

        public UserOutput Operator { get; }

        public TimeSpan Time { get; }

        public string AreaId { get;  }

        public string Tag { get; }

        public string PID { get; }

        public string Description { get; }

        public bool? MaintenanceRequired { get; }

        public string MaintenanceDescription { get; }

        public string MaximoRefNo { get; }

        public string Comments { get; }

        public ShiftLogOutput(
            Guid logId,
            UserOutput @operator,
            TimeSpan time,
            string areaId,
            string tag,
            string pid,
            string decription,
            bool? maintenanceRequired,
            string maintenanceDescription,
            string maximoRefNo,
            string comments)
        {
            LogId = logId;
            Operator = @operator;
            Time = time;
            AreaId = areaId;
            Tag = tag;
            PID = pid;
            Description = Description;
            MaintenanceRequired = maintenanceRequired;
            MaintenanceDescription = maintenanceDescription;
            MaximoRefNo = maximoRefNo;
            Comments = comments;
        }
    }
}
