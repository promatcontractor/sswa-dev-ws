﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Application
{
    public sealed class ShiftOutput
    {
        public Guid ShiftId { get; }

        public DateTime Date { get; }

        public string ShiftType { get; }

        public decimal? Swell { get; }

        public decimal? MaxForecaseWindSpeed { get; }

        public TimeSpan? LowTideTime { get; }

        public decimal? RainForecast { get; }

        public string Comments { get; }

        public string OHSEComments { get; }

        public UserOutput Operator { get; }

        public List<ShiftLogOutput> Logs { get; }

        public ShiftOutput(
            Guid shiftId, 
            DateTime date, 
            string shiftType,
            decimal? swell,
            decimal? maxForecastWindSpeed,
            TimeSpan? lowTideTime,
            decimal? rainForecast,
            string comments,
            string ohseComments,
            UserOutput @operator,
            List<ShiftLogOutput> logs)
        {
            ShiftId = shiftId;
            Date = date;
            ShiftType = shiftType;
            Swell = swell;
            MaxForecaseWindSpeed = maxForecastWindSpeed;
            LowTideTime = lowTideTime;
            RainForecast = rainForecast;
            Comments = comments;
            OHSEComments = ohseComments;
            Operator = @operator;
            Logs = logs;
        }

    }
}
