﻿using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Application
{
    public sealed class ProjectOutput
    {
        public Guid ProjectId { get; }

        public string Name { get;  }

        public ProjectOutput(Guid projectId, string name)
        {
            ProjectId = projectId;
            Name = name;
        }

        public ProjectOutput(Project project)
        {
            ProjectId = project.Id;
            Name = project.Name;
        }
    }
}
