﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.GetShiftByOperator
{
    public interface IGetShiftByOperatorUseCase
    {
        Task<ShiftOutput> Execute(DateTime? date, string shiftType, Guid? operatorId);
    }
}
