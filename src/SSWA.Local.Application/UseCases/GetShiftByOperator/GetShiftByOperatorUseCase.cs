﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.GetShiftByOperator
{
    public sealed class GetShiftByOperatorUseCase : IGetShiftByOperatorUseCase
    {
        public async Task<ShiftOutput> Execute(DateTime? date, string shiftType, Guid? operatorId)
        {
            return new ShiftOutput(
                Guid.NewGuid(),
                DateTime.Now,
                "Day",
                12.12m,
                10.8m,
                new TimeSpan(8, 0, 0),
                null,
                "Comments",
                "OHSE Comments",
                new UserOutput("", "Name Name", null),
                new List<ShiftLogOutput>()
                {
                    new ShiftLogOutput(
                        Guid.NewGuid(),
                        new UserOutput("", "Name Name", null),
                        new TimeSpan(0,0,2),
                        "1",
                        "TAG001",
                        "PID--001",
                        "Description",
                        true,
                        "Maintenance Decription",
                        "MAXIMO--001",
                        "Comments"),
                    new ShiftLogOutput(
                        Guid.NewGuid(),
                        new UserOutput("", "Name Name", null),
                        new TimeSpan(0,0,8),
                        "1",
                        "TAG002",
                        "PID--001",
                        "Description",
                        false,
                        null,
                        "MAXIMO--001",
                        "Comments"),
                    new ShiftLogOutput(
                        Guid.NewGuid(),
                        new UserOutput("", "Name Name", null),
                        new TimeSpan(0,0,8),
                        "1",
                        "TAG003",
                        "PID--003",
                        "Description",
                        null,
                        null,
                        "MAXIMO--001",
                        "Comments"),
                });


        }
    }
}
