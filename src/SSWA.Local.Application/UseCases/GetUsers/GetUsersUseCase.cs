﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Users;

namespace SSWA.Local.Application.UseCases.GetUsers
{
    public sealed class GetUsersUseCase : IGetUsersUseCase
    {
        private readonly IUserReadOnlyRepository _repo;

        public GetUsersUseCase(IUserReadOnlyRepository repo)
        {
            this._repo = repo;
        }

        public Task<IReadOnlyCollection<User>> Execute()
        {
            return _repo.GetAll();
        }
    }
}
