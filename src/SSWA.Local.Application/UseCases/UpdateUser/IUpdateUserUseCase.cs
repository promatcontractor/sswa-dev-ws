﻿using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.UpdateUser
{
    public interface IUpdateUserUseCase
    {
        Task Execute(User user);
    }
}
