﻿using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.UpdateUser
{
    public sealed class UpdateUserUseCase : IUpdateUserUseCase
    {
        private readonly IUserWriteOnlyRepository _repo;

        public UpdateUserUseCase(IUserWriteOnlyRepository repo)
        {
            this._repo = repo;
        }

        public Task Execute(User user)
        {
            return _repo.Update(user);
        }
    }
}
