﻿using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.CreateProject
{
    public sealed class CreateProjectUseCase : ICreateProjectUseCase
    {
        private readonly IProjectWriteOnlyRepository _projectWriteOnlyRepository;

        public CreateProjectUseCase(IProjectWriteOnlyRepository projectWriteOnlyRepository)
        {
            _projectWriteOnlyRepository = projectWriteOnlyRepository;
        }

        public async Task<CreateProjectOutput> Execute(string name)
        {
            Project project = new Project(name, 1);

            await _projectWriteOnlyRepository.Add(project);

            CreateProjectOutput output = new CreateProjectOutput(project);
            return output;
        }
    }
}
