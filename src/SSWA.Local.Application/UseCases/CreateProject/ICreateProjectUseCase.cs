﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.CreateProject
{
    public interface ICreateProjectUseCase
    {
        Task<CreateProjectOutput> Execute(string name);

    }
}
