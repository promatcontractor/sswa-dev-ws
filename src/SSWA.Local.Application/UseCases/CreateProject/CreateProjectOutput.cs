﻿using SSWA.Local.Core.Projects;

namespace SSWA.Local.Application.UseCases.CreateProject
{
    public sealed class CreateProjectOutput
    {
        public ProjectOutput Project { get; } 

        public CreateProjectOutput(Project project)
        {
            Project = new ProjectOutput(project);
        }
    }
}