﻿using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.FirstLogin
{
    public interface IFirstLoginUseCase
    {
        Task Execute(User user);
    }
}
