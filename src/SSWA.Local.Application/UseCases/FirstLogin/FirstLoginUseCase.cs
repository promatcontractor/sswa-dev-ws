﻿using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.FirstLogin
{
    public sealed class FirstLoginUseCase : IFirstLoginUseCase
    {
        private readonly IUserReadOnlyRepository _userReadOnlyRepository;
        private readonly IUserWriteOnlyRepository _userWriteOnlyRepository;

        public FirstLoginUseCase(
            IUserReadOnlyRepository userReadOnlyRepository,
            IUserWriteOnlyRepository userWriteOnlyRepository)
        {
            _userReadOnlyRepository = userReadOnlyRepository;
            _userWriteOnlyRepository = userWriteOnlyRepository;
        }

        public async Task Execute(User user)
        {
            User existingUser = await _userReadOnlyRepository.Get(user.Id);

            if (existingUser == null)
            {
                user.AddRole("operator");
                await _userWriteOnlyRepository.Add(user);
            }
        }
    }
}
