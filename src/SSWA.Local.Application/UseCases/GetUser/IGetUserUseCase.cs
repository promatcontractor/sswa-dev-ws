﻿using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.GetUser
{
    public interface IGetUserUseCase
    {
        Task<User> Execute(string id);
    }
}
