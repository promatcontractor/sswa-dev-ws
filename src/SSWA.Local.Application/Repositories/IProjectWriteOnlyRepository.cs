﻿using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IProjectWriteOnlyRepository
    {
        Task Add(Project project);
        Task Update(Project project);
        Task Delete(Project project);
    }
}
