﻿using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IUserWriteOnlyRepository
    {
        Task Add(User user);
        Task Update(User user);
        Task Delete(User user);
    }
}
