﻿using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IProjectReadOnlyRepository
    {
        Task<IReadOnlyCollection<Project>> GetAll();
        Task<Project> Get(Guid id);
    }
}
