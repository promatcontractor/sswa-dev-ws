﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace SSWA.Local.Core.Users
{
    public sealed class RolesCollection
    {
        private readonly IList<string> _roles;

        public RolesCollection()
        {
            _roles = new List<string>();
        }
        
        public IReadOnlyCollection<string> GetRoles()
        {
            IReadOnlyCollection<string> roles = new ReadOnlyCollection<string>(_roles);
            return roles;
        }

        public void Add(string role)
        {
            _roles.Add(role);
        }
    }
}
