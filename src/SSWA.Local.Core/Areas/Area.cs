﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Areas
{
    public sealed class Area : IEntity<Guid>, IAggregateRoot<Guid>
    {
        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public string Reference { get; private set; }

        public Guid? ProjectId { get; private set; }

        public Area(string name, string reference)
        {
            Id = Guid.NewGuid();
            Name = name;
            Reference = reference;
        }

        private Area() { }

        public static Area Load(Guid id, string name, string reference, Guid? projectId)
        {
            Area area = new Area
            {
                Id = id,
                Name = name,
                Reference = reference,
                ProjectId = projectId
            };

            return area;
        }
    }
}
