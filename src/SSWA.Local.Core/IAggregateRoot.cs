﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core
{
    internal interface IAggregateRoot<T> : IEntity<T>
    {
    }
}
