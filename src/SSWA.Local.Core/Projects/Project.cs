﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Projects
{
    public sealed class Project : IEntity<Guid>, IAggregateRoot<Guid>
    {
        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public int Reference { get; private set; }

        public Project(string name, int reference)
        {
            Id = Guid.NewGuid();
            Name = name;
            Reference = reference;
        }

        private Project() {  }

        public static Project Load(Guid id, string name, int reference)
        {
            Project project = new Project
            {
                Id = id,
                Name = name,
                Reference = reference
            };

            return project;
        }
    }
}
