﻿using Microsoft.AspNetCore.Http;
using SSWA.Local.Application.Services;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace SSWA.Local.Infrastructure.AspNetCoreUser
{
    public sealed class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        const string BASE_CLAIM_NAME = "http://api.sswa/claims/";
        const string NAME_CLAIM_NAME = BASE_CLAIM_NAME + "name";
        const string EMAIL_CLAIM_NAME = BASE_CLAIM_NAME + "email";
        const string ROLE_CLAIM_NAME = BASE_CLAIM_NAME + "roles";
        const string PERMISSION_CLAIM_NAME = BASE_CLAIM_NAME + "permissions";

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public User GetUser()
        {
            User user = User.Load(Id, 0, Name, Roles);
            return user;
        }

        private string Id => _httpContextAccessor.HttpContext.User?.Claims
                            .FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?
                            .Value;

        private string Name
        {
            get
            {
                string firstName = _httpContextAccessor.HttpContext.User?.Claims
                                .FirstOrDefault(c => c.Type == ClaimTypes.GivenName)?
                                .Value;

                string lastName = _httpContextAccessor.HttpContext.User?.Claims
                                .FirstOrDefault(c => c.Type == ClaimTypes.Surname)?
                                .Value;

                return $"{firstName} {lastName}";
            }
        }

        private RolesCollection Roles {
            get
            {
               RolesCollection rolesCollection = new RolesCollection();

               IEnumerable<string> roles = _httpContextAccessor.HttpContext.User?.Claims
                    .Where(c => c.Type == ClaimTypes.Role)
                    .Select(c => c.Value);


                foreach (var role in roles)
                {
                    rolesCollection.Add(role);
                }

                return rolesCollection;
            }
        }


        private string Email => _httpContextAccessor.HttpContext.User?.Claims
                            .FirstOrDefault(c => c.Type == ClaimTypes.Email)?
                            .Value;
    }
}
