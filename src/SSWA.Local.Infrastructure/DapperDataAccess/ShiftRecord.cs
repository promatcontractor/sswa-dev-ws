﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.Data
{
    public class ShiftRecord
    {
        public int? ShiftId { get; set; }

        public DateTime Date { get; set; }

        public string ShiftType { get; set; }

        public string ReviewedBy { get; set; }

        public decimal? Swell { get; set; }

        public decimal? MaxFCastWindSpeed { get; set; }

        public TimeSpan? LowTideTime { get; set; }

        public decimal? RainForecast { get; set; }

        public string Comments { get; set; }

        public string OHSEComments { get; set; }
    }
}
