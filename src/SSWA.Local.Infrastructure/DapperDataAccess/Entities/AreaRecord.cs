﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Entities
{
    public class AreaRecord
    {
        public string AreaId { get; set; }

        public string Area { get; set; }

        public string Title { get; set; }

        public string Comments { get; set; }

        public bool UseArea { get; set; }
    }
}
