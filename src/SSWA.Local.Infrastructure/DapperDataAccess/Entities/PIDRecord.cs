﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.Data
{
    public class PIDRecord
    {
        // Key
        public string PID { get; set; }
        
        public string PIDTitle { get; set; }

        public string Revision { get; set; }

        public DateTime DateModified { get; set; }

        public string Notes { get; set; }

        public int ProjectId { get; set; }
    }
}
