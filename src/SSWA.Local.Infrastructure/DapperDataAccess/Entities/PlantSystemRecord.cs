﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.Data
{
    public class PlantSystemRecord
    {
        // Key
        public string SystemId { get; set; }

        public string SystemName { get; set; }

        public string Notes { get; set; }

        public string Area { get; set; }
    }
}
